from ticketing_app.models import UserProfile

def user_data(request):
    try:
        profile = UserProfile.objects.get(user=request.user)
        full_name = f'{profile.first_name} {profile.last_name}' 
    except:
        full_name = ''
    res = {"USER_FULL_NAME": full_name}
    return res