from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

import ticketing_app.views

urlpatterns = [
    path('admin/', admin.site.urls),

    path('', ticketing_app.views.home_page, name='home_page'),
    path('ticket', ticketing_app.views.ticket, name='ticket'),
    path('ticket_history', ticketing_app.views.ticket_history, name='ticket_history'),
    path('signup', ticketing_app.views.signup, name='signup'),
    path('user_login', ticketing_app.views.user_login, name='user_login'),
    path('user_logout', ticketing_app.views.user_logout, name='user_logout'),


] +  static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
