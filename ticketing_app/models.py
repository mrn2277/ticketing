from django.db import models
from django.contrib.auth.models import User

class Company(models.Model):
    name = models.CharField('name', max_length=150)

    def __str__(self):
        return self.name


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField('first name', max_length=100)
    last_name = models.CharField('lastt name', max_length=100)
    company = models.ForeignKey(Company, on_delete=models.SET_NULL, null=True)
    email = models.EmailField('email', max_length=150, unique=True)
    phone_number = models.CharField('phone number', max_length=11, unique=True)

    def __str__(self):
        return self.first_name + ' ' + self.last_name

class TicketType(models.Model):
    name = models.CharField('name', max_length=100)

    def __str__(self):
        return self.name

class Ticket(models.Model):
    user = models.ForeignKey(UserProfile, on_delete=models.CASCADE, null=True)
    ticket_type = models.ForeignKey(TicketType, on_delete=models.CASCADE)
    title = models.CharField('title', max_length=100)
    description = models.TextField()
    datetime = models.DateTimeField('datetime', auto_now_add=True)