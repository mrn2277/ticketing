from django.shortcuts import render, redirect
from ticketing_app.models import *
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.urls import reverse, reverse_lazy
from django.http import JsonResponse
from django.db.models import F

def signup(request):
    print('requst.POST:', dict(request.POST))
    context = {
        'first_name': request.POST.get('first_name', ''),
        'last_name': request.POST.get('last_name', ''),
    }
    if 'signup_submit' in request.POST:
        user = User.objects.create_user(
            username=request.POST['email'], 
            password=request.POST['password'], 
        )
        user.save()
        profile = UserProfile(
            first_name=request.POST['first_name'],
            last_name=request.POST['last_name'],
            email=request.POST['email'],
            phone_number=request.POST.get('phone', 'None'),
            company=Company.objects.get(pk=request.POST['company']),
            user=user
        )
        profile.save()
        login(request, user)
        print('--------user logged in successfully----------------')
        context.update({'logged_in': True})
       
        print('--------user created successfully----------------')

    context.update({
        'companies': Company.objects.all()
    })
    return render(request, 'signup.html', context)


def user_login(request):
    print('user:', request.user)
    print('requst.GET:', dict(request.GET))
    context = {}
    if 'login_submit' in request.POST:
        auth_user = authenticate(username=request.POST['email'], password=request.POST['password'])
        if auth_user is not None:
            login(request, auth_user)
            print('logged in successfully')
            # return redirect()
        else:
            context.update({"not_authenticate": True})

    return render(request, 'login.html', context)

def user_logout(request):
    logout(request)
    return redirect(reverse('user_login'))


def home_page(request):
    context = {}
    return render(request, 'home_page.html', context)

@login_required(login_url=reverse_lazy('user_login'))
def ticket(request):
    print('requst.POST:', dict(request.POST))
    print('requst.GET:', dict(request.GET))
    ticket_types = TicketType.objects.all()
    context = {'ticket_types': ticket_types}
    if 'title' in request.POST:
        ticket_type = TicketType.objects.get(name=request.POST['type'])
        ticket = Ticket(
            user = UserProfile.objects.get(user=request.user),
            title = request.POST['title'],
            ticket_type = ticket_type,
            description = request.POST['description']
        )
        ticket.save()
        context.update({'success': True, 'ticket_id': ticket.id})

    return render(request, 'ticket.html', context)


@login_required(login_url=reverse_lazy('user_login'))
def ticket_history(request):
    context = {
        'title': request.POST.get('title', ''),
        'ticket_type': request.POST.get('ticket_type', '')
    }
    user_profile = UserProfile.objects.get(user=request.user)
    user_tickets = Ticket.objects.filter(user=user_profile)

    # if 'get_page_data' in request.POST:
    #     page_number = int(request.POST['page_number'])
    #     sending_tickets = user_tickets.annotate(
    #         tickettype=F('ticket_type__name')
    #     ).values( 
    #         'title', 'description', 'datetime', 'tickettype'
    #     )[(page_number - 1)*3 + 1: page_number * 3 + 1]
        # return JsonResponse({'tickets': list(sending_tickets)})

    user_tickets = Ticket.objects.filter(user=user_profile)
    if 'title' in request.POST:
        user_tickets = user_tickets.filter(title__contains=request.POST['title'])
    
    if 'ticket_type' in request.POST and request.POST['ticket_type']:
        user_tickets = user_tickets.filter(ticket_type__name=request.POST['ticket_type'])

    # page_numbers = int(user_tickets.count()/3) + 1 # some comment
    # page_range = range(1, page_numbers + 1)
    ticket_types = TicketType.objects.all()
    context.update({'tickets': user_tickets, 'ticket_types': ticket_types})
    return render(request, 'ticket_history.html', context)


