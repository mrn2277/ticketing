# Generated by Django 3.1.1 on 2020-09-23 20:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='TicketType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='name')),
            ],
        ),
        migrations.CreateModel(
            name='Ticket',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100, verbose_name='title')),
                ('description', models.TextField()),
                ('datetime', models.DateTimeField(auto_now_add=True, verbose_name='datetime')),
                ('ticket_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ticketing_app.tickettype')),
            ],
        ),
    ]
