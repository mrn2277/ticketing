# Generated by Django 3.1.1 on 2020-10-01 06:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ticketing_app', '0002_company_userprofile'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='email',
            field=models.EmailField(max_length=150, unique=True, verbose_name='email'),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='phone_number',
            field=models.CharField(max_length=11, unique=True, verbose_name='phone number'),
        ),
    ]
